FROM alpine:3.7

RUN  apk add --update --no-cache openssh-server iproute2 bridge-utils python3 shadow

RUN ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""
ADD sshd_config /etc/ssh/sshd_config

ADD motd /etc/motd

RUN mkdir /sshvpn
ADD requirements.txt /sshvpn

WORKDIR /sshvpn
RUN pip3 install --requirement requirements.txt

ADD ssh-vpn-server.py /sshvpn

CMD ["python3", "/sshvpn/ssh-vpn-server.py", "/etc/ssh-vpn/ssh-vpn-config.yaml"]
