import yaml
import argparse
import os
import uuid


def make_tuntap(user, bridge):
    create_command = 'ip tuntap add {mode}{did} mode {mode} user {username}'.format(**user)
    bridge_command = 'ip link set {mode}{did} master {bridge}'.format(**user, bridge=bridge)
    up_command = 'ip link set {mode}{did} up'.format(**user)

    os.system(create_command)
    os.system(bridge_command)
    os.system(up_command)


def setup_user(user):
    create_user_command = 'useradd -ms /bin/false {username}'.format(**user)
    os.system(create_user_command)

    user['password'] = uuid.uuid4()
    setpw_command = "echo '{username}:{password}' | chpasswd".format(**user)
    os.system(setpw_command)

    ssh_dir = '/home/{username}/.ssh'.format(**user)
    os.makedirs(ssh_dir, exist_ok=True)

    with open('{}/authorized_keys'.format(ssh_dir), 'w') as f:
        f.write(user['publickey'])
        f.write('\n')


def run_server():
    os.system('/usr/sbin/sshd -e -D')


def main(args):
    if not os.path.exists('/dev/net'):
        os.makedirs('/dev/net')
        os.system('mknod /dev/net/tun c 10 200')
        os.system('chmod 666 /dev/net/tun')

    with open(args.config) as f:
        config = yaml.load(f)

    for u in config['users']:
        setup_user(u)
        make_tuntap(u, config['bridge']['name'])

    run_server()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    args = parser.parse_args()
    main(args)
